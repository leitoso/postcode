defmodule CEP.Neighbourhoods do
  @moduledoc """
  Documentation for `CEP.Neighbourhoods`.
  """

  alias CEP.{Utils, Neighbourhoods}

  defstruct has_neighbourhoods?:  false,
            neighbourhoods:       %{},
            total_neighbourhoods: 0,
            timeout:              900,
            stats?:               false,
            cache:                nil

  @neighbourhoods_url "https://sidra.ibge.gov.br/Territorio/Unidades?nivel=102"

  @doc """
  Get neighbourhoods
  """

  def get(
    %CEP{all?: all?, neighbourhoods?: neighbourhoods?} = struct
  ) when all? and not neighbourhoods? do
    struct
  end

  @doc """
  Get neighbourhoods
  """

  def get(%CEP{
    all?: all?,
    neighbourhoods?: neighbourhoods?,
    cache_exists?: cache_exists?,
    cache: cache,
    stats?: stats?,
    timeout: timeout
  } = struct) when all? or neighbourhoods? do
    sections = if cache_exists?, do: Utils.list_sections(cache), else: []

    %{ neighbourhoods: neighbourhoods } =
      %Neighbourhoods{
        timeout: timeout,
        cache: cache,
        stats?: stats?,
        has_neighbourhoods?: neighbourhoods? and Enum.member?(sections, "neighbourhoods")
      }
      |> load_neighbourhoods_from_cache
      |> get_neighbourhoods
      |> stats(struct)

    #%CEP{ struct | neighbourhoods: neighbourhoods }

    ""
  end

  def get_neighbourhoods(
    %Neighbourhoods{has_neighbourhoods?: has_neighbourhoods?} = struct
  ) when has_neighbourhoods? do
    struct
  end

  def get_neighbourhoods(
    %Neighbourhoods{has_neighbourhoods?: has_neighbourhoods?, cache: cache, timeout: timeout} = struct
  ) when not has_neighbourhoods? do
    case HTTPoison.get(@neighbourhoods_url, [], timeout: 15_000, recv_timeout: timeout * 1_000) do
      {:ok, %{status_code: 200, body: body}} ->
        Utils.write_cache(body, cache, :neighbourhoods)
        |> Poison.decode
        |> parse_neighbourhoods(struct)

      {:ok, %{status_code: status}} ->
        exit {:error, "something went wrong, http code ##{status}"}

      {:error, %{reason: reason}} ->
        exit {:error, "something else went really wrong, reason: #{reason}"}
    end
  end

  def load_neighbourhoods_from_cache(
    %Neighbourhoods{has_neighbourhoods?: has_neighbourhoods?} = struct
  ) when not has_neighbourhoods? do
    struct
  end

  def load_neighbourhoods_from_cache(
    %Neighbourhoods{has_neighbourhoods?: has_neighbourhoods?, cache: cache} = struct
  ) when has_neighbourhoods? do
    Utils.read_cache(cache, :neighbourhoods) |> parse_neighbourhoods(struct)
  end

  def parse_neighbourhoods a, b, c, d, e \\ [], f \\ 0
  def parse_neighbourhoods(
    [neighbor | n_tail], [municip | m_tail], [state | s_tail], struct, n_cache, total
  ) do
    parse_neighbourhoods(
      n_tail,
      m_tail,
      s_tail,
      struct,
      [%{key: "#{municip}_#{state}", municipality: municip, neighbourhood: neighbor, state: state} | n_cache],
      total + 1
    )
  end

  def parse_neighbourhoods [], [], [], struct, n_cache, total do
    %Neighbourhoods{struct |
      neighbourhoods:       Enum.group_by(n_cache, & &1[:key]),
      total_neighbourhoods: total,
      has_neighbourhoods?:  true
    }
  end

  def parse_neighbourhoods(
    {:ok, %{"Nivel"=>%{"Id"=>102}, "Nomes"=>neighbourhoods, "SiglasUF"=>states, "Complementos1"=>municipalities}},
    struct
  ) do
    parse_neighbourhoods neighbourhoods, municipalities, states, struct
  end

  def stats(%{stats?: stats?} = struct, _) when not stats?, do: struct
  def stats(
    %{neighbourhoods: neighbourhoods, total_neighbourhoods: total, stats?: stats?} = struct,
    %{municipalities?: municipalities?}
  ) when stats? and not municipalities? do
    total_stats(neighbourhoods, total)
    |> IO.puts

    struct
  end

  def stats(
    %{neighbourhoods: neighbourhoods, total_neighbourhoods: total, stats?: stats?} = struct,
    %{municipalities: municipalities, municipalities?: municipalities?}
  ) when stats? and municipalities? do
    total_stats neighbourhoods, total

    neighbours_set = Map.keys(neighbourhoods)
    |> Enum.into(MapSet.new)

    diff = Enum.into(municipalities, MapSet.new)
    |> MapSet.difference(neighbours_set)

    IO.puts "Total municipalities without neighbourhoods: #{Enum.count(diff)}"
    IO.puts ""

    struct
  end

  def total_stats(municipalities, neighbourhoods) when is_map(municipalities) do
    Map.keys(municipalities)
    |> Enum.count
    |> total_stats(neighbourhoods)
  end

  def total_stats(municipalities, neighbourhoods) when is_number(municipalities) do
    IO.puts "Total neighbourhoods found: #{neighbourhoods}"
    IO.puts "Total municipalities with neighbourhoods: #{municipalities}"
    ""
  end
end
